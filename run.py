# coding=utf-8
from __future__ import print_function
import redis
import time
import pickle
from bson.objectid import ObjectId
from multiprocessing import Pool
import settings
import os


# from shujuqingxi.data import save_styles, save_runways, save_runways_images, save_an_image, save_bo_image, \
#     save_in_image, save_pa_image, save_re_image, save_tr_image


def push_items(name, coll_name):
    from settings import NUM_MONGO_DATA, YEAR
    num = NUM_MONGO_DATA

    from shujuqingxi.mongo_models import dict_coll
    coll = dict_coll.get(name)
    if not coll:
        return

    redis_client = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, password=settings.REDIS_PSW, db=3)

    from shujuqingxi.mongo_models import client as mongodb_client
    coll_object_id = mongodb_client['Spider_pop_objectID']['object_id']
    while True:
        if YEAR:
            items_1 = coll.find({'updateTime': {'$regex': '{}.*'.format(YEAR)}})
            items_2 = coll.find({'update_time': {'$regex': '{}.*'.format(YEAR)}})
            if items_1.count():
                items = items_1
            else:
                items = items_2
        else:
            items = coll.find()
        if items.count() > 0:
            num_count = 0
            while True:
                # o_id = '000000000000000000000000'
                try:
                    item = items.next()
                    o_id = str(item['_id'])[:8] + '0' * 16
                    item_id = coll_object_id.find_one({'_id': item['_id']})
                    if not item_id:
                        while True:
                            len_redis_list = redis_client.llen(coll_name)
                            if len_redis_list < 1000:
                                break
                            else:
                                # print('等待')
                                time.sleep(1)
                        redis_client.rpush(coll_name, pickle.dumps(item))
                        # redis_client.rpush(coll_name, item)

                        coll_object_id.insert(
                            {'_id': item['_id'], 'generation_time': str(item['_id'])[:8], 'o_id': o_id})

                        num_count += 1
                        if num_count >= num > 0:
                            return
                except Exception as e:
                    if num > 0:
                        return
                    items = coll.find({'_id': {'$gt': ObjectId(o_id)}})
                    time.sleep(10)
        else:
            if num > 0:
                return
            time.sleep(10)


def p_maker(num):
    pool = Pool(50)
    save_list = [
        save_styles if settings.CLEAN_STYLE_DATA else None,
        save_runways if settings.CLEAN_SHOW_DATA else None,
        save_runways_images if settings.CLEAN_SHOW_DATA else None,
        save_an_image if settings.CLEAN_OTHER_DATA else None,
        save_bo_image if settings.CLEAN_OTHER_DATA else None,
        save_in_image if settings.CLEAN_OTHER_DATA else None,
        save_pa_image if settings.CLEAN_OTHER_DATA else None,
        save_re_image if settings.CLEAN_OTHER_DATA else None,
        save_tr_image if settings.CLEAN_OTHER_DATA else None,
    ]
    for save_p in save_list:
        if not save_p:
            continue
        for i in range(num):
            pool.apply_async(func=save_p)

    return pool


def run():
    num = settings.PROCESS_NUM
    print('Process:{}倍'.format(num))
    pool = p_maker(num)
    coll_list = [
        ('coll_ru_272', 'save_runways') if settings.CLEAN_SHOW_DATA else None,
        ('coll_ru_323', 'save_runways') if settings.CLEAN_SHOW_DATA else None,
        ('coll_ru_335', 'save_runways') if settings.CLEAN_SHOW_DATA else None,
        ('coll_ru_341', 'save_runways') if settings.CLEAN_SHOW_DATA else None,
        ('coll_ru_other', 'save_runways') if settings.CLEAN_SHOW_DATA else None,
        ('coll_ru_image', 'save_runways_images') if settings.CLEAN_SHOW_DATA else None,
        ('coll_st_image', 'save_styles') if settings.CLEAN_STYLE_DATA else None,
        ('coll_an_image', 'save_an_image') if settings.CLEAN_OTHER_DATA else None,
        ('coll_bo_image', 'save_bo_image') if settings.CLEAN_OTHER_DATA else None,
        ('coll_in_image', 'save_in_image') if settings.CLEAN_OTHER_DATA else None,
        ('coll_pa_image', 'save_pa_image') if settings.CLEAN_OTHER_DATA else None,
        ('coll_re_image', 'save_re_image') if settings.CLEAN_OTHER_DATA else None,
        ('coll_tr_image', 'save_tr_image') if settings.CLEAN_OTHER_DATA else None,
    ]
    for coll_p in coll_list:
        if not coll_p:
            continue
        pool.apply_async(func=push_items, args=coll_p)

    return pool


if __name__ == '__main__':
    import argparse
    import pymongo

    mongodb_client = pymongo.MongoClient(host=settings.MONGO_HOST, port=settings.MONGO_PORT)
    if settings.MONGO_USER and settings.MONGO_PSW:
        mongodb_client.admin.authenticate(settings.MONGO_USER, settings.MONGO_PSW)

    parser = argparse.ArgumentParser(description='manual to this script')
    parser.add_argument('--table', type=str, default=None)
    parser.add_argument('--num', type=int, default=None)
    parser.add_argument('--process', type=int, default=None)
    parser.add_argument('--begin', type=str, default=None)
    args = parser.parse_args()
    table = args.table
    if table == 'product':
        settings.CLEAN_STYLE_DATA = True
        settings.CLEAN_SHOW_DATA = False
        settings.CLEAN_OTHER_DATA = False
    if table == 'show':
        settings.CLEAN_STYLE_DATA = False
        settings.CLEAN_SHOW_DATA = True
        settings.CLEAN_OTHER_DATA = False
    if table == 'other':
        settings.CLEAN_STYLE_DATA = False
        settings.CLEAN_SHOW_DATA = False
        settings.CLEAN_OTHER_DATA = True
    if table == 'all':
        settings.CLEAN_STYLE_DATA = True
        settings.CLEAN_SHOW_DATA = True
        settings.CLEAN_OTHER_DATA = True
    if args.num:
        settings.NUM_MONGO_DATA = args.num
    if args.process:
        settings.PROCESS_NUM = args.process
    if args.begin:
        settings.BEGIN = eval(args.begin)

    if settings.BEGIN:
        mongodb_client.drop_database('Spider_pop_objectID')
        print(u'从头清洗')
    else:
        print(u'继续清洗\n')
    print(u'程序版本：v4.8')
    print(u'清洗{}表'.format(table if table else u'配置文件中的'))
    print(u'数量为{}'.format(args.num if args.num else u'配置文件中的设置'))
    print(u'{}倍进程'.format(settings.PROCESS_NUM))
    print(u'缩略图设置为{}'.format(settings.IMAGES_THUMBS))
    print(u'正在清洗。。。')
    from shujuqingxi.data import save_styles, save_runways, save_runways_images, save_an_image, save_bo_image, \
        save_in_image, save_pa_image, save_re_image, save_tr_image

    print('start')
    pool = run()
    print('watching')
    os.system('python watching.py')
    pool.terminate()
    pool.join()
