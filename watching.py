# coding=utf-8
import settings
import redis
import pymongo
import codecs
import time
import os

mongodb_client = pymongo.MongoClient(host=settings.MONGO_HOST, port=settings.MONGO_PORT)
if settings.MONGO_USER and settings.MONGO_PSW:
    mongodb_client.admin.authenticate(settings.MONGO_USER, settings.MONGO_PSW)

coll_object_id = mongodb_client['Spider_pop_objectID']['object_id']

redis_client = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, password=settings.REDIS_PSW, db=3)


def db_count(db_name):
    db = mongodb_client[db_name]
    cols = db.collection_names()
    count = 0
    for col in cols:
        count += db[col].count()
    return count


def year_count(db_name):
    db = mongodb_client[db_name]
    cols = db.collection_names()
    count = 0
    for col in cols:
        n1 = db[col].find({'updateTime': {'$regex': '{}.*'.format(settings.YEAR)}}).count()
        if n1:
            count += n1
        else:
            n2 = db[col].find({'update_time': {'$regex': '{}.*'.format(settings.YEAR)}}).count()
            count += n2
    return count


if os.path.exists('./log/m.txt'):
    with codecs.open('./log/m.txt', 'r', encoding='utf-8') as f:
        c_num = int(f.read())
else:
    c_num = 0


def redis_count():
    save_runways = redis_client.llen('save_runways')
    save_runways_images = redis_client.llen('save_runways_images')
    save_styles = redis_client.llen('save_styles')
    save_an_image = redis_client.llen('save_an_image')
    save_bo_image = redis_client.llen('save_bo_image')
    save_in_image = redis_client.llen('save_in_image')
    save_pa_image = redis_client.llen('save_pa_image')
    save_re_image = redis_client.llen('save_re_image')
    save_tr_image = redis_client.llen('save_tr_image')

    count_redis = save_runways + save_runways_images + save_styles + save_an_image + save_bo_image + save_in_image + save_pa_image + save_re_image + save_tr_image

    return count_redis


def w_run():
    count_object_id = db_count('Spider_pop_objectID') - redis_count()
    if settings.NUM_MONGO_DATA:
        count_object_id -= c_num
    while True:
        count_mongodb = db_count('Spider_pop_images') + db_count('Spider_pop_runways')
        if settings.YEAR:
            count_mongodb = year_count('Spider_pop_images') + year_count('Spider_pop_runways')
        if settings.NUM_MONGO_DATA:
            count_mongodb_new = 0
            if settings.CLEAN_STYLE_DATA:
                count_mongodb_new += settings.NUM_MONGO_DATA
            if settings.CLEAN_SHOW_DATA:
                count_mongodb_new += settings.NUM_MONGO_DATA * 2
            if settings.CLEAN_OTHER_DATA:
                count_mongodb_new += settings.NUM_MONGO_DATA * 6

            if count_mongodb > count_mongodb_new:
                count_mongodb = count_mongodb_new

        count_redis = redis_count()
        num_count_object_id = count_object_id
        count_object_id = db_count('Spider_pop_objectID') - count_redis

        with codecs.open('./log/m.txt', 'w', encoding='utf-8') as f:
            f.write(str(count_object_id))

        if settings.NUM_MONGO_DATA:
            count_object_id -= c_num

        if not count_mongodb:
            count_mongodb = 1
        count = (count_object_id - count_redis) * 100 / count_mongodb

        per_count = count_object_id - num_count_object_id
        with codecs.open('./log/watching.log', 'a', encoding='utf-8') as f:
            f.write(u'数据清洗进度：{}%，每秒清洗{}条\n'.format(count, per_count))
        time.sleep(1)


if __name__ == '__main__':
    w_run()
