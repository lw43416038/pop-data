# coding=utf-8
import pymongo
import settings

client = pymongo.MongoClient(host=settings.MONGO_HOST, port=settings.MONGO_PORT)
if settings.MONGO_USER and settings.MONGO_PSW:
    client.admin.authenticate(settings.MONGO_USER, settings.MONGO_PSW)
db_image = client[settings.MONGO_DB_IMAGE]

coll_bo_image = db_image[settings.MONGO_COLL_BOOKS_IMAGES]
coll_in_image = db_image[settings.MONGO_COLL_INSP_IMAGES]
coll_pa_image = db_image[settings.MONGO_COLL_PATTERNS_IMAGES]
coll_st_image = db_image[settings.MONGO_COLL_STYLES_IMAGES]
coll_re_image = db_image[settings.MONGO_COLL_REFER_IMAGES]
coll_tr_image = db_image[settings.MONGO_COLL_TRENDS_IMAGES]
coll_an_image = db_image[settings.MONGO_COLL_ANALYSIS_IMAGES]
coll_ru_image = db_image[settings.MONGO_COLL_RUNWAYS_IMAGES]

# db_st = client[settings.MONGO_DB_STYLES]  # 获得数据库的句柄
db_ru = client[settings.MONGO_DB_RUNWAYS]  # 获得数据库的句
#
# coll_st_ru = db_st[settings.MONGO_COLL_STYLES_RUNWAYS]  # 获得collection的句柄
# coll_st_sh = db_st[settings.MONGO_COLL_STYLES_SHOWS]  # 获得collection的句柄
# coll_st_de = db_st[settings.MONGO_COLL_STYLES_DESIGN]  # 获得collection的句柄
# coll_st_on = db_st[settings.MONGO_COLL_STYLES_ONLINE]  # 获得collection的句柄
# coll_st_po = db_st[settings.MONGO_COLL_STYLES_POPULAR]  # 获得collection的句柄
# coll_st_re = db_st[settings.MONGO_COLL_STYLES_RETAIL]  # 获得collection的句柄
# coll_st_tr = db_st[settings.MONGO_COLL_STYLES_TREND]  # 获得collection的句柄
# coll_st_st = db_st[settings.MONGO_COLL_STYLES_STREET]  # 获得collection的句柄
#
coll_ru_272 = db_ru[settings.MONGO_COLL_RUNWAYS_272]  # 获得collection的句柄
coll_ru_341 = db_ru[settings.MONGO_COLL_RUNWAYS_341]  # 获得collection的句柄
coll_ru_335 = db_ru[settings.MONGO_COLL_RUNWAYS_335]  # 获得collection的句柄
coll_ru_323 = db_ru[settings.MONGO_COLL_RUNWAYS_323]  # 获得collection的句柄
coll_ru_other = db_ru[settings.MONGO_COLL_RUNWAYS_OTHER]  # 获得collection的句柄

dict_coll = {
    'coll_an_image': coll_an_image,
    'coll_bo_image': coll_bo_image,
    'coll_in_image': coll_in_image,
    'coll_pa_image': coll_pa_image,
    'coll_re_image': coll_re_image,
    'coll_tr_image': coll_tr_image,
    'coll_st_image': coll_st_image,
    'coll_ru_image': coll_ru_image,

    'coll_ru_272': coll_ru_272,
    'coll_ru_341': coll_ru_341,
    'coll_ru_335': coll_ru_335,
    'coll_ru_323': coll_ru_323,
    'coll_ru_other': coll_ru_other,
}
