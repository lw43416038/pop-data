# coding=utf-8
from sqlalchemy import BIGINT, Column, INTEGER, String, TIMESTAMP, Text, text, create_engine, SMALLINT, Integer, \
    BigInteger,DateTime
from sqlalchemy.dialects.mysql.types import TINYINT
from sqlalchemy.dialects.mysql import LONGTEXT
from sqlalchemy.dialects.mysql.enumerated import ENUM
from sqlalchemy.ext.declarative import declarative_base
import settings

Base_B = declarative_base()
Base_STYLE = declarative_base()
Base_SHOW = declarative_base()
Base_OTHER = declarative_base()


class Brand(Base_B):
    __tablename__ = 'brand'

    id = Column(INTEGER, primary_key=True)
    name = Column(String(100, 'utf8mb4_unicode_ci'), default='')
    title = Column(String(100, 'utf8mb4_unicode_ci'), default='')
    spell = Column(String(50, 'utf8mb4_unicode_ci'), server_default=text("''"))
    style = Column(String(100, 'utf8mb4_unicode_ci'), server_default=text("''"))
    src = Column(String(10, 'utf8mb4_unicode_ci'), default='pop')
    brand_level_id = Column(TINYINT(4), server_default=text("'0'"))
    parent_id = Column(INTEGER, default=0)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    status = Column(TINYINT(4), server_default=text("'1'"))


if settings.CLEAN_STYLE_DATA:
    class SrcProduct(Base_STYLE):
        __tablename__ = 'src_product'
        # __table_args__ = (
        #     Index('itemid', 'itemid', 'sitename', unique=True),
        # )

        id = Column(INTEGER, primary_key=True)
        itemid = Column(BIGINT, default=0)
        name = Column(String(150), default='')
        sitename = Column(ENUM('farfetch', 'netaporter', '', 'taobao', 'tmall', 'mytheresa', 'test', 'pop'),
                          server_default=text("''"))
        brand_name = Column(String(50), default='')
        cate_id = Column(INTEGER, server_default=text("'0'"))
        brand_id = Column(INTEGER, index=True, server_default=text("'0'"))
        org_price = Column(String(20), default='')
        org_size = Column(String(200), server_default=text("''"))
        org_catepath = Column(String(100), default='')
        org_cateid = Column(INTEGER, server_default=text("'0'"))
        org_shopid = Column(INTEGER, server_default=text("'0'"))
        user_tags = Column(String(600), server_default=text("''"))
        org_desc = Column(Text, default='')
        org_desc2 = Column(String(500), default='')
        org_desc3 = Column(String(500), default='')
        catename = Column(String(50), default='')
        url = Column(String(250), default='')
        org_extend = Column(Text, default='')
        img_num = Column(TINYINT(4), server_default=text("'-1'"))
        add_time = Column(INTEGER, nullable=False, default=0)
        update_time = Column(INTEGER, server_default=text("'0'"))
        status = Column(TINYINT(4), server_default=text("'1'"))


    class SrcProductImage(Base_STYLE):
        __tablename__ = 'src_product_images'

        id = Column(INTEGER, primary_key=True)
        product_id = Column(INTEGER, nullable=False, default=0)
        localurl = Column(String(200), nullable=False, default='')
        checksum = Column(String(100), nullable=False, server_default=text("''"))
        org_alt = Column(String(100), nullable=False, server_default=text("''"))
        org_index = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
        width = Column(SMALLINT, nullable=False, server_default=text("'0'"))
        height = Column(SMALLINT, nullable=False, server_default=text("'0'"))
        url = Column(String(300), nullable=False, default='')
        add_time = Column(INTEGER, nullable=False, default=0)
        cloudstatus = Column(TINYINT(4), nullable=False, server_default=text("'1'"))
        status = Column(TINYINT(4), nullable=False, server_default=text("'1'"))

if settings.CLEAN_SHOW_DATA:
    class Show(Base_SHOW):
        __tablename__ = 'shows'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(356), nullable=False, default='')
        sitename = Column(ENUM('vogue', 'pop', ''), nullable=False, server_default=text("''"))
        type = Column(ENUM('adult', 'senior', 'swimwear', 'wedding', ''), server_default=text("''"))
        brand_id = Column(INTEGER, nullable=False, default=0)
        brand_name = Column(String(100), nullable=False, default='')
        season = Column(String(100), nullable=False, default='')
        gender = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
        date = Column(String(50), default='')
        city = Column(String(50), default='')
        pic_path = Column(String(1000), default='')
        source_url = Column(String(255), nullable=False, default='')
        created_at = Column(TIMESTAMP, default='2018-09-01 00:00:00')
        updated_at = Column(TIMESTAMP, default='2018-09-01 00:00:00')
        status = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


    class ShowImage(Base_SHOW):
        __tablename__ = 'show_image'

        id = Column(Integer, primary_key=True)
        show_id = Column(Integer, nullable=False)
        title = Column(String(255))
        url = Column(String(255, 'utf8mb4_unicode_ci'))
        localurl = Column(String(255))
        checksum = Column(String(255))
        type = Column(ENUM('collection', 'detail', 'beauty', 'atmosphere'), server_default=text("'collection'"))
        org_alt = Column(String(100, 'utf8mb4_unicode_ci'), nullable=False, server_default=text("''"))
        size = Column(BigInteger, nullable=False, server_default=text("'0'"))
        width = Column(Integer)
        height = Column(Integer)
        created_at = Column(DateTime)
        updated_at = Column(DateTime)
        cloudstatus = Column(Integer)
        status = Column(Integer, nullable=False, server_default=text("'0'"))

if settings.CLEAN_OTHER_DATA:
    class Analysi(Base_OTHER):
        __tablename__ = 'analysis'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(356))
        url = Column(String(255))
        html = Column(LONGTEXT)
        author = Column(String(100))
        updatetime = Column(String(255))
        label = Column(String(255))
        desc = Column(Text)


    class Book(Base_OTHER):
        __tablename__ = 'books'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(255))
        bigpath = Column(String(356))
        desc = Column(Text)
        booknumber = Column(String(100))
        url = Column(String(255))
        updatetime = Column(String(100))
        shape = Column(String(255))


    class Inspiration(Base_OTHER):
        __tablename__ = 'inspiration'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(255))
        desc = Column(Text)
        updatetime = Column(String(100))
        images = Column(Text)
        uid = Column(String(50))


    class Pattern(Base_OTHER):
        __tablename__ = 'patterns'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(255))
        url = Column(String(255))
        content = Column(LONGTEXT)


    class Reference(Base_OTHER):
        __tablename__ = 'references'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(255))
        updatetime = Column(String(100))
        label = Column(String(255))
        url = Column(String(255))
        bigpath = Column(String(255))


    class Trend(Base_OTHER):
        __tablename__ = 'trends'

        id = Column(INTEGER, primary_key=True)
        title = Column(String(255))
        desc = Column(Text)
        author = Column(String(100))
        updatetime = Column(String(100))
        url = Column(String(255))
        label = Column(String(255))
        html = Column(LONGTEXT)

if settings.CREATE_ALL_TABLES:
    engine = create_engine(
        "mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(settings.MYSQL_BRANDS_USER,
                                                                settings.MYSQL_BRANDS_PSW,
                                                                settings.MYSQL_BRANDS_HOST,
                                                                settings.MYSQL_BRANDS_PORT,
                                                                settings.MYSQL_BRANDS_DB))
    Base_B.metadata.create_all(engine)

    if settings.CLEAN_STYLE_DATA:
        engine1 = create_engine(
            "mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(settings.MYSQL_STYLES_USER,
                                                                    settings.MYSQL_STYLES_PSW,
                                                                    settings.MYSQL_STYLES_HOST,
                                                                    settings.MYSQL_STYLES_PORT,
                                                                    settings.MYSQL_STYLES_DB))
        Base_STYLE.metadata.create_all(engine1)
    if settings.CLEAN_SHOW_DATA:
        engine2 = create_engine(
            "mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(settings.MYSQL_SHOWS_USER,
                                                                    settings.MYSQL_SHOWS_PSW,
                                                                    settings.MYSQL_SHOWS_HOST,
                                                                    settings.MYSQL_SHOWS_PORT,
                                                                    settings.MYSQL_SHOWS_DB))
        Base_SHOW.metadata.create_all(engine2)
    if settings.CLEAN_OTHER_DATA:
        engine3 = create_engine(
            "mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(settings.MYSQL_OTHERS_USER,
                                                                    settings.MYSQL_OTHERS_PSW,
                                                                    settings.MYSQL_OTHERS_HOST,
                                                                    settings.MYSQL_OTHERS_PORT,
                                                                    settings.MYSQL_OTHERS_DB))
        Base_OTHER.metadata.create_all(engine3)
# session_factory = sessionmaker(bind=engine, autocommit=True)
# Session = scoped_session(session_factory)
# session = scoped_session(sessionmaker(bind=engine, autocommit=True))()
