# coding=utf-8
from __future__ import print_function
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from uploadBos import UploadImages
import json
import time
import redis
import pickle
from datetime import datetime
import settings
import threading

if settings.CLEAN_STYLE_DATA:
    from .models import Brand, SrcProduct, SrcProductImage
if settings.CLEAN_SHOW_DATA:
    from .models import Brand, Show, ShowImage
if settings.CLEAN_OTHER_DATA:
    from .models import Analysi, Book, Inspiration, Pattern, Trend, Reference

redis_client = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, password=settings.REDIS_PSW, db=3)


def save_styles():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_STYLES_USER, settings.MYSQL_STYLES_PSW,
        settings.MYSQL_STYLES_HOST, settings.MYSQL_STYLES_PORT,
        settings.MYSQL_STYLES_DB
    ), pool_size=10, max_overflow=20)
    engine_brand = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_BRANDS_USER, settings.MYSQL_BRANDS_PSW,
        settings.MYSQL_BRANDS_HOST, settings.MYSQL_BRANDS_PORT,
        settings.MYSQL_BRANDS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))
    Session_brand = scoped_session(sessionmaker(bind=engine_brand, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        session_brand = Session_brand()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_styles')[1]
            i = pickle.loads(i)
            item = {"itemid": i["id"], "name": i["stitle"],
                    "brand_name": i["brandInfo"]["name"] if i.get("brandInfo") else ""}
            obj = session_brand.query(Brand).filter_by(name=item["brand_name"]).first()
            if obj:
                item["brand_id"] = obj.id
            else:
                b = {"created_at": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                     "updated_at": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                     "name": item["brand_name"],
                     "src": "pop"}
                session_brand.bulk_insert_mappings(Brand, [b])
                # session.commit()
                obj = session_brand.query(Brand).filter_by(name=item["brand_name"]).first()
                item["brand_id"] = obj.id

            org_catepath_list = []
            [org_catepath_list.append(j["name"]) if j["name"] in settings.STYLE_TYPE else None for j in i["styleInfo"]]
            org_catepath = ';'.join(org_catepath_list)
            org_desc = ','.join(list(set(org_catepath_list) ^ set([j["name"] for j in i["styleInfo"]])))

            item["org_catepath"] = org_catepath
            item["org_desc"] = org_desc
            item["catename"] = org_catepath_list[-1]
            item["sitename"] = 'pop'
            item["url"] = i["url"]
            item["img_num"] = 0
            item["add_time"] = int(time.time())
            item["update_time"] = int(time.time())
            obj = session.query(SrcProduct).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(SrcProduct, [item])
            else:
                oid = obj.id
                item.pop("add_time")
                item["id"] = oid
                session.bulk_update_mappings(SrcProduct, [item])
            # session.commit()
            img_num = 0
            for k in i["detailList"]:
                item1 = {}
                url = k["bigPath"]
                if url:
                    item1["url"] = url
                    result = upload_image.get_image(url)
                    if result:
                        item1["localurl"], item1["checksum"] = result
                    else:
                        continue
                img_num += 1
                obj = session.query(SrcProduct).filter_by(itemid=k["id"]).first()
                item1["product_id"] = obj.id
                item1["cloudstatus"] = 9
                # item1["org_alt"] = json.dumps([j["name"] for j in i["styleInfo"]], ensure_ascii=False)
                try:
                    item1["width"] = k["shape"].split("*")[0]
                    item1["height"] = k["shape"].split("*")[1]
                except:
                    pass
                item1["add_time"] = int(time.time())
                obj = session.query(SrcProductImage).filter_by(url=item1["url"]).first()
                if not obj:
                    session.bulk_insert_mappings(SrcProductImage, [item1])
                    # session.commit()
                else:
                    oid = obj.id
                    item1.pop("add_time")
                    item1["id"] = oid
                    session.bulk_update_mappings(SrcProductImage, [item1])

            obj = session_brand.query(SrcProduct).filter_by(url=i["url"]).first()
            if obj:
                item_2 = {"id": obj.id, "img_num": img_num, "update_time": int(time.time())}
                session.bulk_update_mappings(SrcProduct, [item_2])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(30)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_runways():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_SHOWS_USER, settings.MYSQL_SHOWS_PSW,
        settings.MYSQL_SHOWS_HOST, settings.MYSQL_SHOWS_PORT,
        settings.MYSQL_SHOWS_DB
    ), pool_size=10, max_overflow=20)
    engine_brand = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_BRANDS_USER, settings.MYSQL_BRANDS_PSW,
        settings.MYSQL_BRANDS_HOST, settings.MYSQL_BRANDS_PORT,
        settings.MYSQL_BRANDS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))
    Session_brand = scoped_session(sessionmaker(bind=engine_brand, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        session_brand = Session_brand()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_runways')[1]
            i = pickle.loads(i)
            item = {"title": i["nme"], "brand_name": i["brand_name"]}
            if u"成衣" in i["category_text"]:
                item["type"] = "adult"
            elif u"高级" in i["category_text"]:
                item["type"] = "senior"
            elif u"泳装" in i["category_text"]:
                item["type"] = "swimwear"
            elif u"婚纱" in i["category_text"]:
                item["type"] = "wedding"
            else:
                item["type"] = ""
            obj = session_brand.query(Brand).filter_by(name=i["brand_name"]).first()
            if obj:
                item["brand_id"] = obj.id
                item["brand_name"] = obj.name
            else:
                b = {"created_at": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                     "updated_at": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                     "name": item["brand_name"],
                     "src": "pop"}
                session_brand.bulk_insert_mappings(Brand, [b])
                # session.commit()
                obj = session_brand.query(Brand).filter_by(name=i["brand_name"]).first()
                item["brand_id"] = obj.id
                item["brand_name"] = obj.name
            item["season"] = i["seasonName"]
            if i["genderName"].strip() == u"女装":
                item["gender"] = 2
            elif i["genderName"].strip() == u"男装":
                item["gender"] = 1
            else:
                item["gender"] = 3
            item["date"] = i["create_time"]
            item["city"] = i["regionName"]
            item["sitename"] = 'pop'
            pic_path = eval(i["sCoverImg"])
            item["pic_path"] = str([upload_image.get_image(i)[0] for i in pic_path])
            item["source_url"] = "http://www.pop-fashion.com/runways/inside/id_{}/".format(i["id"])
            item["created_at"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            item["updated_at"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            obj = session.query(Show).filter_by(source_url=item["source_url"]).first()
            if not obj:
                session.bulk_insert_mappings(Show, [item])
                # session.commit()
            else:
                oid = obj.id
                item.pop("created_at")
                item["id"] = oid
                session.bulk_update_mappings(Show, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_runways_images():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_SHOWS_USER, settings.MYSQL_SHOWS_PSW,
        settings.MYSQL_SHOWS_HOST, settings.MYSQL_SHOWS_PORT,
        settings.MYSQL_SHOWS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_runways_images')[1]
            i = pickle.loads(i)
            item = {}
            url = i["detailList"][0]["bigPath"]
            if url:
                item["url"] = url
                result = upload_image.get_image(url)
                if result:
                    item["localurl"], checksum = result
                else:
                    continue
            obj = session.query(Show).filter_by(
                source_url="http://www.pop-fashion.com/runways/inside/id_{}/".format(i["listId"])).first()
            if obj:
                item["show_id"] = obj.id
            else:
                session.bulk_insert_mappings(Show, [
                    {"source_url": "http://www.pop-fashion.com/runways/inside/id_{}/".format(i["listId"])}])
                # session.commit()
                obj = session.query(Show).filter_by(
                    source_url="http://www.pop-fashion.com/runways/inside/id_{}/".format(i["listId"])).first()
                item["show_id"] = obj.id

            item["type"] = i["images_type"]
            item["title"] = i["stitle"]
            size = i["detailList"][0]["size"]
            if size:
                if 'MB' in size:
                    item["size"] = int(size.replace('MB', '')) * 1024
                if 'KB' in size:
                    item["size"] = int(size.replace('KB', ''))

            try:
                wh_list = i["detailList"][0]["shape"].split("*")
                item["width"] = int(wh_list[0])
                item["height"] = int(wh_list[1])
            except:
                pass
            item["created_at"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            item["updated_at"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            obj = session.query(ShowImage).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(ShowImage, [item])
                # session.commit()
            else:
                oid = obj.id
                item.pop("created_at")
                item["id"] = oid
                session.bulk_update_mappings(ShowImage, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(30)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_an_image():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_OTHERS_USER, settings.MYSQL_OTHERS_PSW,
        settings.MYSQL_OTHERS_HOST, settings.MYSQL_OTHERS_PORT,
        settings.MYSQL_OTHERS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_an_image')[1]
            i = pickle.loads(i)
            item = {"title": i["title"], "url": i["url"], "html": i["content_html"], "author": i["author"],
                    "updatetime": i["update_time"], "label": ",".join(i["key_words"]), "desc": i["desc"]}
            obj = session.query(Analysi).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(Analysi, [item])
                # session.commit()
            else:
                oid = obj.id
                item["id"] = oid
                session.bulk_update_mappings(Analysi, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_bo_image():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_OTHERS_USER, settings.MYSQL_OTHERS_PSW,
        settings.MYSQL_OTHERS_HOST, settings.MYSQL_OTHERS_PORT,
        settings.MYSQL_OTHERS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_bo_image')[1]
            i = pickle.loads(i)
            item = {"title": i["stitle"], "bigpath": i["detailList"][0]["bigPath"], "desc": i["description"],
                    "booknumber": i.get("bookNumber"), "url": i["url"], "updatetime": i["updateTime"],
                    "shape": i["detailList"][0]["shape"]}
            # item["booknumber"]=i["bookNumber"] if i["bookNumber"] else None
            obj = session.query(Book).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(Book, [item])
                # session.commit()
            else:
                oid = obj.id
                item["id"] = oid
                session.bulk_update_mappings(Book, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_in_image():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_OTHERS_USER, settings.MYSQL_OTHERS_PSW,
        settings.MYSQL_OTHERS_HOST, settings.MYSQL_OTHERS_PORT,
        settings.MYSQL_OTHERS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_in_image')[1]
            i = pickle.loads(i)
            item = {"title": i["title"], "desc": i["desc"], "updatetime": i["update_time"],
                    "images": json.dumps(i["image"], ensure_ascii=False), "uid": i["id"]}
            obj = session.query(Inspiration).filter_by(uid=item["uid"]).first()
            if not obj:
                session.bulk_insert_mappings(Inspiration, [item])
                # session.commit()
            else:
                oid = obj.id
                item["id"] = oid
                session.bulk_update_mappings(Inspiration, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_pa_image():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_OTHERS_USER, settings.MYSQL_OTHERS_PSW,
        settings.MYSQL_OTHERS_HOST, settings.MYSQL_OTHERS_PORT,
        settings.MYSQL_OTHERS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_pa_image')[1]
            i = pickle.loads(i)
            item = {"title": i.get("stitle")}
            if not item["title"]:
                item["title"] = i.get("title")
            item["url"] = i["url"]
            i.pop("_id")
            item["content"] = json.dumps(i, ensure_ascii=False)
            obj = session.query(Pattern).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(Pattern, [item])
                # session.commit()
            else:
                oid = obj.id
                item["id"] = oid
                session.bulk_update_mappings(Pattern, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_re_image():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_OTHERS_USER, settings.MYSQL_OTHERS_PSW,
        settings.MYSQL_OTHERS_HOST, settings.MYSQL_OTHERS_PORT,
        settings.MYSQL_OTHERS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_re_image')[1]
            i = pickle.loads(i)
            item = {"title": i["stitle"], "updatetime": i["updateTime"],
                    "label": json.dumps([j["name"] for j in i["styleInfo"]], ensure_ascii=False), "url": i["url"],
                    "bigpath": i["detailList"][0]["bigPath"]}
            obj = session.query(Reference).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(Reference, [item])
                # session.commit()
            else:
                oid = obj.id
                item["id"] = oid
                session.bulk_update_mappings(Reference, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)


def save_tr_image():
    engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_OTHERS_USER, settings.MYSQL_OTHERS_PSW,
        settings.MYSQL_OTHERS_HOST, settings.MYSQL_OTHERS_PORT,
        settings.MYSQL_OTHERS_DB
    ), pool_size=10, max_overflow=20)
    Session_mysql = scoped_session(sessionmaker(bind=engine, autocommit=True))

    def run():
        upload_image = UploadImages()
        session = Session_mysql()
        while True:
            if redis_client.get('stop'):
                return
            i = redis_client.blpop('save_tr_image')[1]
            i = pickle.loads(i)
            item = {"title": i["title"], "desc": i["desc"], "author": i["author"], "updatetime": i["update_time"],
                    "url": i["url"], "label": ",".join(i["key_words"]), "html": i["content_html"]}
            obj = session.query(Trend).filter_by(url=item["url"]).first()
            if not obj:
                session.bulk_insert_mappings(Trend, [item])
                # session.commit()
            else:
                oid = obj.id
                item["id"] = oid
                session.bulk_update_mappings(Trend, [item])
                # session.commit()

    try:
        pool = [threading.Thread(target=run) for i in xrange(15)]
        [p.start() for p in pool]
        [p.join() for p in pool]
    except Exception as e:
        settings.logger.error(e)
