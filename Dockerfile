FROM python:2.7.15-jessie
ADD . /code
WORKDIR /code
RUN rm -f images/test.txt images/full/test.txt log/test.txt && pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
RUN cd bce-python-sdk-0.8.23/ && python setup.py install