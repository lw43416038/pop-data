# coding=utf-8
import os
import logging
from baidubce.bce_client_configuration import BceClientConfiguration
from baidubce.auth.bce_credentials import BceCredentials
from baidubce.services.bos.bos_client import BosClient

BOS_HOST = os.environ['BOS_HOST'] if 'BOS_HOST' in os.environ else 'http://su.bcebos.com'
BOS_AK = os.environ['BOS_AK'] if 'BOS_AK' in os.environ else '0ee68e81c7f94b2685ce0a2a1d9705e6'
BOS_SK = os.environ['BOS_SK'] if 'BOS_SK' in os.environ else '132994492265449fa53b16a7ef73d39a'

BOS_BUCKET = os.environ['BOS_BUCKET'] if 'BOS_BUCKET' in os.environ else '58fd-test'
BOS_IA = eval(os.environ['BOS_IA']) if 'BOS_IA' in os.environ else False

CREATE_ALL_TABLES = eval(os.environ['CREATE_ALL_TABLES']) if 'CREATE_ALL_TABLES' in os.environ else False
CLEAN_STYLE_DATA = eval(os.environ['CLEAN_STYLE_DATA']) if 'CLEAN_STYLE_DATA' in os.environ else False
CLEAN_SHOW_DATA = eval(os.environ['CLEAN_SHOW_DATA']) if 'CLEAN_SHOW_DATA' in os.environ else False
CLEAN_OTHER_DATA = eval(os.environ['CLEAN_OTHER_DATA']) if 'CLEAN_OTHER_DATA' in os.environ else False
NUM_MONGO_DATA = int(os.environ['NUM_MONGO_DATA']) if 'NUM_MONGO_DATA' in os.environ else 0
YEAR = os.environ['YEAR'] if 'YEAR' in os.environ else None
PROCESS_NUM = int(os.environ['PROCESS_NUM']) if 'PROCESS_NUM' in os.environ else 2
BEGIN = eval(os.environ['BEGIN']) if 'BEGIN' in os.environ else False

IMAGES_THUMBS = eval(os.environ['IMAGES_THUMBS']) if 'IMAGES_THUMBS' in os.environ else {'80_80': (80, 80),
                                                                                         '400_400': (400, 400)}

IMAGES_STORE = os.path.abspath(os.path.dirname(__file__))
IMAGES_LOCAL_SAVE = eval(os.environ['IMAGES_LOCAL_SAVE']) if 'IMAGES_LOCAL_SAVE' in os.environ else False

MONGO_HOST = os.environ['MONGO_HOST'] if 'MONGO_HOST' in os.environ else None  # 主机IP
MONGO_PORT = int(os.environ['MONGO_PORT']) if 'MONGO_PORT' in os.environ else 27017  # 端口号
MONGO_USER = os.environ['MONGO_USER'] if 'MONGO_USER' in os.environ else None
MONGO_PSW = os.environ['MONGO_PSW'] if 'MONGO_PSW' in os.environ else None

REDIS_HOST = 'redis'
REDIS_PORT = 6379
REDIS_PSW = None

MYSQL_ALL_HOST = os.environ['MYSQL_ALL_HOST'] if 'MYSQL_ALL_HOST' in os.environ else None
MYSQL_ALL_PORT = int(os.environ['MYSQL_ALL_PORT']) if 'MYSQL_ALL_PORT' in os.environ else 3306
MYSQL_ALL_USER = os.environ['MYSQL_ALL_USER'] if 'MYSQL_ALL_USER' in os.environ else None
MYSQL_ALL_PSW = os.environ['MYSQL_ALL_PSW'] if 'MYSQL_ALL_PSW' in os.environ else None
MYSQL_ALL_DB = os.environ['MYSQL_ALL_DB'] if 'MYSQL_ALL_DB' in os.environ else None

MYSQL_STYLES_HOST = os.environ['MYSQL_STYLES_HOST'] if 'MYSQL_STYLES_HOST' in os.environ else MYSQL_ALL_HOST
MYSQL_STYLES_PORT = int(os.environ['MYSQL_STYLES_PORT']) if 'MYSQL_STYLES_PORT' in os.environ else MYSQL_ALL_PORT
MYSQL_STYLES_USER = os.environ['MYSQL_STYLES_USER'] if 'MYSQL_STYLES_USER' in os.environ else MYSQL_ALL_USER
MYSQL_STYLES_PSW = os.environ['MYSQL_STYLES_PSW'] if 'MYSQL_STYLES_PSW' in os.environ else MYSQL_ALL_PSW
MYSQL_STYLES_DB = os.environ['MYSQL_STYLES_DB'] if 'MYSQL_STYLES_DB' in os.environ else MYSQL_ALL_DB

MYSQL_SHOWS_HOST = os.environ['MYSQL_SHOWS_HOST'] if 'MYSQL_SHOWS_HOST' in os.environ else MYSQL_ALL_HOST
MYSQL_SHOWS_PORT = int(os.environ['MYSQL_SHOWS_PORT']) if 'MYSQL_SHOWS_PORT' in os.environ else MYSQL_ALL_PORT
MYSQL_SHOWS_USER = os.environ['MYSQL_SHOWS_USER'] if 'MYSQL_SHOWS_USER' in os.environ else MYSQL_ALL_USER
MYSQL_SHOWS_PSW = os.environ['MYSQL_SHOWS_PSW'] if 'MYSQL_SHOWS_PSW' in os.environ else MYSQL_ALL_PSW
MYSQL_SHOWS_DB = os.environ['MYSQL_SHOWS_DB'] if 'MYSQL_SHOWS_DB' in os.environ else MYSQL_ALL_DB

MYSQL_BRANDS_HOST = os.environ['MYSQL_BRANDS_HOST'] if 'MYSQL_BRANDS_HOST' in os.environ else MYSQL_ALL_HOST
MYSQL_BRANDS_PORT = int(os.environ['MYSQL_BRANDS_PORT']) if 'MYSQL_BRANDS_PORT' in os.environ else MYSQL_ALL_PORT
MYSQL_BRANDS_USER = os.environ['MYSQL_BRANDS_USER'] if 'MYSQL_BRANDS_USER' in os.environ else MYSQL_ALL_USER
MYSQL_BRANDS_PSW = os.environ['MYSQL_BRANDS_PSW'] if 'MYSQL_BRANDS_PSW' in os.environ else MYSQL_ALL_PSW
MYSQL_BRANDS_DB = os.environ['MYSQL_BRANDS_DB'] if 'MYSQL_BRANDS_DB' in os.environ else MYSQL_ALL_DB

MYSQL_OTHERS_HOST = os.environ['MYSQL_OTHERS_HOST'] if 'MYSQL_OTHERS_HOST' in os.environ else MYSQL_ALL_HOST
MYSQL_OTHERS_PORT = int(os.environ['MYSQL_OTHERS_PORT']) if 'MYSQL_OTHERS_PORT' in os.environ else MYSQL_ALL_PORT
MYSQL_OTHERS_USER = os.environ['MYSQL_OTHERS_USER'] if 'MYSQL_OTHERS_USER' in os.environ else MYSQL_ALL_USER
MYSQL_OTHERS_PSW = os.environ['MYSQL_OTHERS_PSW'] if 'MYSQL_OTHERS_PSW' in os.environ else MYSQL_ALL_PSW
MYSQL_OTHERS_DB = os.environ['MYSQL_OTHERS_DB'] if 'MYSQL_OTHERS_DB' in os.environ else MYSQL_ALL_DB

MONGO_DB_RUNWAYS = "Spider_pop_runways"  # 库名
MONGO_COLL_RUNWAYS_272 = "runways_reg_272"
MONGO_COLL_RUNWAYS_341 = "runways_reg_341"
MONGO_COLL_RUNWAYS_335 = "runways_reg_335"
MONGO_COLL_RUNWAYS_323 = "runways_reg_323"
MONGO_COLL_RUNWAYS_OTHER = "runways_reg_other"

MONGO_DB_IMAGE = "Spider_pop_images"
MONGO_COLL_BOOKS_IMAGES = "books_images"
MONGO_COLL_INSP_IMAGES = "inspiration_images"
MONGO_COLL_PATTERNS_IMAGES = "patterns_images"
MONGO_COLL_STYLES_IMAGES = "styles_images"
MONGO_COLL_REFER_IMAGES = "references_images"
MONGO_COLL_TRENDS_IMAGES = "trends_images"
MONGO_COLL_ANALYSIS_IMAGES = "analysis_images"
MONGO_COLL_RUNWAYS_IMAGES = "runways_images"

STYLE_TYPE = [
    u'套装', u'长袜/连裤袜', u'袜子', u'直身婚纱', u'A字型婚纱', u'帽子', u'风衣', u'斗篷／披肩', u'睡衣', u'比基尼', u'抹胸', u'亲子装', u'外套', u'晚礼服',
    u'太阳帽', u'T恤', u'背带裙', u'泳裤', u'下装/内裤', u'围巾', u'背带裤', u'夹克', u'情趣内衣', u'背心', u'皮手套', u'民族服装', u'耳饰', u'套衫', u'衬衫',
    u'内衣', u'首饰', u'专业竞技泳衣', u'家居装', u'束身衣', u'领带', u'分体泳衣', u'口水兜', u'鱼尾婚纱', u'吊带', u'礼服', u'西装套装', u'婚纱', u'沙滩裤',
    u'领结', u'短袜', u'半身裙', u'套装式', u'旗袍', u'套头帽', u'连衣裤', u'包屁裤', u'其他', u'中长裤', u'上衣/文胸', u'手饰', u'大衣', u'手套', u'裙',
    u'保暖系列', u'polo衫', u'泳装', u'连体泳衣', u'卫衣', u'短裤', u'家居服', u'吊牌', u'裤', u'卫裤', u'皮革/皮草帽', u'开衫', u'眼镜', u'孕妇装',
    u'小礼服', u'大内衣', u'连衣裙', u'马甲', u'西装', u'上衣', u'沙滩裙', u'颈饰', u'棉服/羽绒服', u'男装', u'女装', u'童装'
]

logger = logging.getLogger('baidubce.services.bos.bosclient')
fh = logging.FileHandler(os.path.join(IMAGES_STORE, 'log/wiki.log'))
fh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.setLevel(logging.ERROR)
logger.addHandler(fh)

config = BceClientConfiguration(credentials=BceCredentials(BOS_AK, BOS_SK), endpoint=BOS_HOST)

# 新建BosClient
bos_client = BosClient(config)
